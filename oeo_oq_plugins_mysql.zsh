#!/usr/bin/zsh

# The wrapper zsh function of mysql command. Support flexible MySQL CLI prompt.
#https://github.com/tetsujin/zsh-function-mysql
#
#  Colorization for mysql
# https://github.com/horosgrisa/mysql-colorize

#  ZSH plugin for MySql.
# https://github.com/voronkovich/mysql.plugin.zsh
